FROM openjdk
WORKDIR .
COPY . .
# prendre uniquement le jar
CMD ["./mvnw", "clean", "package", "-DskipTests"]
CMD ["java", "-jar", "./target/quarkus-app/quarkus-run.jar"]
EXPOSE 8080
