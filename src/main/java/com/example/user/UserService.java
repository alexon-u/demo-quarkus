package com.example.user;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.util.List;

@ApplicationScoped
public class UserService {

    @Inject
    UserRepository userRepository;

    public List<User> getUsers(){
        return userRepository.listAll();
    }

    public User getUser(Long id){
        User user =  User.findById(id);
        if(user == null){
            throw new NotFoundException();
        }
        return user;
    }

    @Transactional
    public void createUser(User user){
        userRepository.persist(user);
    }
}
