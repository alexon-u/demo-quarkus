package com.example.user;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class UserRepository implements PanacheRepository<User> {
    // https://quarkus.io/guides/hibernate-orm-panache
    // https://redhat-developer-demos.github.io/quarkus-tutorial/quarkus-tutorial/panache.html
}
