package com.example.user;

import io.micrometer.core.annotation.Counted;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/api/user")
public class UserResource {

    @ConfigProperty(name = "postgresUrl", defaultValue="test")
    String postgtesUrl;

    @Inject
    UserService userService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> getUsers(){
        return userService.getUsers();
    }

    @GET
    @Path("/{id}")
    public User getUser(@PathParam("id") Long id) {
        return userService.getUser(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Counted(value = "custom.newUsers")
    public void createUser(User user){
        userService.createUser(user);
    }

    @GET
    @Path("/postgres")
    public String getPostgtesUrl(){
        return postgtesUrl;
    }
}
