package com.example.user;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="utilisateur")
public class User extends PanacheEntity {
    public String nom;
    public String prenom;
    public String email;
}
